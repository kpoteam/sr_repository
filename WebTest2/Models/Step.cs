﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebTest2.Models
{
    public class Step
    {
		[Key]
		public int Number { get; set; }

		public string Description { get; set; }

		public List<RecipeIngredient> Ingredients { get; set; }

		public FileModel Image { get; set; }

	}
}
