﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTestProject
{
    class IngredientBuilder
    {
        public const string DEFAULT_NAME = "Сахар";
        public const int DEFAULT_CALORIES_COUNT = 150;

        private int? IngredientID;
        private string Name = DEFAULT_NAME;
        private int? CaloriesCount = DEFAULT_CALORIES_COUNT;


        private IngredientBuilder()
        { 
        }

        public static IngredientBuilder aIngredientBuilder()
        {
            return new IngredientBuilder();
        }

        public IngredientBuilder withName(string Name)
        {
            this.Name = Name;
            return this;
        }

        public IngredientBuilder withIngredientID(int? IngredientID)
        {
            this.IngredientID = IngredientID;
            return this;
        }

        public IngredientBuilder withRandIngredientID()
        {
            this.IngredientID = (new Random()).Next() % 5000 + 1;
            return this;
        }

        public IngredientBuilder withCaloriesCount(int? CaloriesCount)
        {
            this.CaloriesCount = CaloriesCount;
            return this;
        }

        public IngredientBuilder withNoCaloriesCount()
        {
            this.CaloriesCount = null;
            return this;
        }

        public IngredientBuilder but()
        {
            return IngredientBuilder.aIngredientBuilder().withIngredientID(IngredientID).withName(Name).withCaloriesCount(CaloriesCount);
        }

        public TestIngridient build()
        {
            return new TestIngridient(IngredientID, Name, CaloriesCount);
        }
    }
}
