﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebTest2.Models
{
    public class Recipe
    {
        public int Id { get; set; }

        public int CreatorId { get; set; }

        public string Name { get; set; }

        public List<RecipeIngredient> IngredientList { get; set; } // список: ингредиент - кол-во в граммах 

        public int Rating { get; set; }

        public string Description { get; set; }

        public int PortionAmount { get; set; }

        public List<Step> Steps { get; set; }

        public FileModel Photo { get; set; }

        public int Minutes { get; set; } // время приготовления в мин. 

        public string Tags { get; set; }

        public string Date { get; set; }
    }
}
