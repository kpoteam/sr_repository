﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebTest2.Models;
using WebTest2.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http.Internal;
using System.Threading;

namespace WebTest2.Controllers
{ 
    public class HomeController : Controller
    {
        ApplicationDbContext db;
        IHostingEnvironment _appEnvironment;
        public HomeController(ApplicationDbContext context, IHostingEnvironment appEnvironment)
        {
            db = context;
            _appEnvironment = appEnvironment;
        }
        public IActionResult Index(string searchString)
        {
            var recipes = from recipe in db.Recipes
                          select recipe;
            if (!String.IsNullOrEmpty(searchString))
            {
                recipes = recipes.Where(s => s.Name.Contains(searchString));
            }

            recipes = recipes.Include(p => p.Photo)
                .Include(p => p.IngredientList).ThenInclude(ingr => ingr.Ingredient)
                .Include(p => p.Steps).ThenInclude(step => step.Ingredients)
                .Include(p => p.Steps).ThenInclude(step => step.Image);
            return View(recipes.ToList());
        }
        [HttpPost]
        public IActionResult Index(string searchString, string searchOption)
        {
            var recipes = from recipe in db.Recipes
                          select recipe;

            if (!String.IsNullOrEmpty(searchString))
            {
                if (searchOption == "recipeName")
                {
                    recipes = recipes.Where(rec => rec.Name.Contains(searchString));
                }
                else if (searchOption == "ingredientName")
                {
                    var searchIngredientList = searchString.Split(',', ';', '|', '.');
                    /*
                    recipes = recipes.Where(rec => rec.IngredientList.Any(
                        recIng => searchIngredientList.Any(
                            ing => ing == recIng.Ingredient.Name)));
                        */
                    recipes = recipes.Where(rec => rec.IngredientList.Any(
                        recIng => recIng.Ingredient.Name.Contains(searchIngredientList[0])));

                    for(int i = 1; i < searchIngredientList.Length; i++)
                    {
                        recipes.Union(recipes.Where(rec => rec.IngredientList.Any(
                            recIng => recIng.Ingredient.Name.Contains(searchIngredientList[i]))));
                    }
                }
                else if (searchOption == "tagsName")
                {
                    /*
                    var searchTagsList = searchString.Split(',');
                    recipes = recipes.Where(rec => rec.Tags.Contains(searchTagsList[0]));

                    for (int i = 1; i < searchTagsList.Length; i++)
                    {
                        recipes = recipes.Where(rec => rec.Tags.Contains(searchTagsList[i]));
                    }
                    */
                }
            }
            recipes = recipes.Include(p => p.Photo)
                .Include(p => p.IngredientList).ThenInclude(ingr => ingr.Ingredient)
                .Include(p => p.Steps).ThenInclude(step => step.Ingredients)
                .Include(p => p.Steps).ThenInclude(step => step.Image);
            return View(recipes.ToList());
        }
        public IActionResult About()
        {
            ViewData["Message"] = "Коротко о приложении:";

            return View();
        }


        public IActionResult Contact()
        {
            ViewData["Message"] = "Наши контакты:";

            return View();
        }

        [HttpGet]
        public IActionResult AddRecipe()
        {
            ViewData["Message"] = "Добавление рецепта:";

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddRecipe(string visibility, string name, string description, 
            int? hours, int? minutes, int? portionAmount, ICollection<RecipeIngredient> mainIngredientsList, 
            IFormFile image, ICollection<IFormFile> stepImages, ICollection<string> imageExist, 
            ICollection<RecipeIngredient> stepIngredientList, ICollection<string> stepDescriptions, string tags)
        {
			// Название рецепта
			string name_;
			if (name != null)
				name_ = name;
			else name_ = "No name";

			// Описание рецепта
			string description_;
			if (description != null)
				description_ = description;
			else description_ = "No description";

			// Часы
			int hours_;
			if (hours != null)
				hours_ = (int)hours;
			else hours_ = 0;

			// Минуты
			int minutes_;
			if (minutes != null)
				minutes_ = (int)minutes;
			else minutes_ = 0;

			// Количество порций
			int portionAmount_;
			if (portionAmount != null)
				portionAmount_ = (int)portionAmount;
			else portionAmount_ = 1;

			// Все ингредиенты рецепта
			var recipeIngredientList = new List<RecipeIngredient>();
            for (int i = 0; i < mainIngredientsList.Count(); i++)
				recipeIngredientList.Add(
					new RecipeIngredient
					{
						Amount = mainIngredientsList.ElementAt(i).Amount,
						// TODO: Реализовать поиск ингредиента в базе, создание нового, если еще не существует 
						Ingredient = new Ingredient
						{
							Name = mainIngredientsList.ElementAt(i).Ingredient.Name,
							CaloriesCount = 0
						},
						Type = mainIngredientsList.ElementAt(i).Type
					});

			// Фотограция рецепта
			FileModel mainPhoto = null;
            if (image != null)
            {
                string id_name = ((db.Files.Count() == 0) ? 0 : db.Files.Last().Id) + 1 + "_" + image.FileName;
                // путь к папке Files
                string path = "/Files/" + id_name;
                // сохраняем файл в папку Files в каталоге wwwroot
                using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
                {
                    await image.CopyToAsync(fileStream);
                }
				mainPhoto = new FileModel { Name = id_name, Path = path };
                db.Files.Add(mainPhoto);
                db.SaveChanges();
			}
			else
			{
				// Необходимо существование этой картинки
				mainPhoto = new FileModel { Name = "no_photo.png", Path = "/images/no_photo.png" };
			}

			var ingredientSteps = new List<Step>();
			for (int i = 0, k = 0; i < stepDescriptions.Count; i++) {
				// Фотография шага
				FileModel stepPhoto = null;
				if(imageExist.ElementAt(i) == "1")
				{
					//элементов в stepImages меньше или равно (null не передается), чем элементов в imageExist, поэтому новый счетчик k
					string id_name = ((db.Files.Count() == 0) ? 0 : db.Files.Last().Id) + 1 + "_" + stepImages.ElementAt(k).FileName;

					// сохраняем файл в папку Files в каталоге wwwroot
					string path = "/Files/" + id_name;
					using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
					{
						await stepImages.ElementAt(k).CopyToAsync(fileStream);
					}
					k++;
					stepPhoto = new FileModel { Name = id_name, Path = path };
					db.Files.Add(stepPhoto);
					db.SaveChanges();
				}
				else
				{
					// Необходимо существование этой картинки
					stepPhoto = new FileModel { Name = "no_photo.png", Path = "/images/ic_upload_photo.png" };
				}
				// Выбираются рецепты, которые принадлежат этом шагу ('i**', 1-я цифра из 3-х - номер шага) и с названием != null
				var stepIngredientList_ = stepIngredientList.Where(x => (Math.Floor(x.Id / 100.0) == i) && (x.Ingredient.Name != null)).ToList();
				foreach (var stepIngr in stepIngredientList_)
					stepIngr.Id = 0;
				ingredientSteps.Add(new Step()
				{
					Description = stepDescriptions.ElementAt(i),
					Image = stepPhoto,
					Ingredients = stepIngredientList_
				});
			}

			// Тэги
			string tags_;
			if (tags != null)
				tags_ = tags;
			else tags_ = string.Empty;


			var recipe = new Recipe
            {
                Name = name_,
				Description = description_,
				Minutes = hours_ * 60 + minutes_,
				PortionAmount = portionAmount_,
				IngredientList = recipeIngredientList,
				Photo = mainPhoto,
				Steps = ingredientSteps,
                Rating = 0,
				Tags = tags,
				Date = DateTime.Now.ToString("yyyyMMddHHmmss")
		};

            db.Recipes.Add(recipe);
            db.SaveChanges();
            return RedirectToAction("Index");
        }



		[HttpPost]
		public IActionResult ShowRecipe(int Id)
		{
			var recipes = db.Recipes.Include(p => p.Photo)
			.Include(p => p.IngredientList).ThenInclude(ingr => ingr.Ingredient)
			.Include(p => p.Steps).ThenInclude(step => step.Ingredients)
			.Include(p => p.Steps).ThenInclude(step => step.Image)
			.Include(x => x.Steps).ThenInclude(step => step.Ingredients).ThenInclude(step => step.Ingredient);
			var recipe = recipes.FirstOrDefault(x => x.Id == Id);
			return View(recipe);
		}


		// TODO: Реализовать поиск ингредиента в базе 
		public Ingredient GetIngredient(string name)
        {
            return null;
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }       
    }
}
