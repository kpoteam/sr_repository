﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnitTestProject
{
    class TestIngridient
    {
        public int? IngredientID;
        public string Name;
        public int? CaloriesCount;

        public TestIngridient(int? IngredientID, string Name, int? CaloriesCount)
        {
            this.IngredientID = IngredientID;
            this.Name = Name;
            this.CaloriesCount = CaloriesCount;
        }

        
    }
}
