using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTests
    {
        [TestMethod]
        public void InitializathionTestMethodAAA()
        {
            // String to Ingredient
            // Arrange
            string testString = "�����";
            var helpsIngredient = new WebTest2.Models.Ingredient();

            // Act
            helpsIngredient.Name = testString;

            // Assert
            Assert.AreEqual(testString, helpsIngredient.Name);


            // Ingredient to RecipeInregient
            // Arrange
            var helpsRecipeIngredient = new WebTest2.Models.RecipeIngredient();

            // Act
            helpsRecipeIngredient.Ingredient = helpsIngredient;

            // Assert
            Assert.AreEqual(testString, helpsRecipeIngredient.Ingredient.Name);


            // RecipeIngredient to Recipe
            // Arrange
            var testObject = new WebTest2.Models.Recipe();
            testObject.IngredientList = new System.Collections.Generic.List<WebTest2.Models.RecipeIngredient>();

            // Act
            testObject.IngredientList.Add(helpsRecipeIngredient);

            // Assert
            Assert.AreEqual(testString, testObject.IngredientList[0].Ingredient.Name);
        }

        // ObjectMother
        class ObjectMother
        {
            public static TestIngridient Sugar()
            {
                return new TestIngridient(111, "�����", 398);
            }

            public static TestIngridient Salt()
            {
                return new TestIngridient(33, "����", 0);
            }
        }

        [TestMethod]
        public void CreationTestMethodOM()
        {
            // Arrange
            var ingredientSugar = ObjectMother.Sugar();
            var ingredientSalt = ObjectMother.Salt();

            Assert.ReferenceEquals(ingredientSugar, ObjectMother.Sugar());
            Assert.ReferenceEquals(ingredientSalt, ObjectMother.Salt());

        }

        [TestMethod]
        public void CreationTestMethodTDB()
        {
            const int cc = 398;
            const int id = 111;

            var ingredientBuilder = IngredientBuilder.aIngredientBuilder().withIngredientID(id).withCaloriesCount(cc);

            var sugar = ingredientBuilder.build();
            var salt = ingredientBuilder.but().withName("����").withNoCaloriesCount().withRandIngredientID().build();


            Assert.IsNotNull(sugar);
            Assert.AreEqual(sugar.Name, "�����");
            Assert.AreEqual(sugar.CaloriesCount, cc);
            Assert.AreEqual(sugar.IngredientID, id);

            Assert.IsNotNull(salt);
            Assert.AreEqual(salt.Name, "����");
            Assert.AreEqual(salt.CaloriesCount, null);
            Assert.IsNotNull(salt.IngredientID);

        }

    }
}
