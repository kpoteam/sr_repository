﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebTest2.Models
{
    public class RecipeIngredient
    {
        [Key]
        public int Id { get; set; }

        public Ingredient Ingredient { get; set; }

        public int Amount { get; set; }

		public string Type { get; set; } // долька/килограмм/грамм/кусок/шт/по вкусу...
	}
}
